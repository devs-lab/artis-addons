/**
 * @file qss/GraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_STAR_ADDONS_QSS_GRAPH_MANAGER
#define ARTIS_STAR_ADDONS_QSS_GRAPH_MANAGER

#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/qss/Derivative.hpp>
#include <artis-star/kernel/qss/Integrator.hpp>
#include <artis-star/kernel/qss/Quantifier.hpp>

namespace artis::addons::qss {

template<class DerivativeParameters>
struct QSSParameters
{
  artis::qss::IntegratorParameters integrator;
  artis::qss::QuantifierParameters quantifier;
  DerivativeParameters derivative;
};

template<class Time,
    class Derivative,
    class DerivativeParameters = artis::common::NoParameters>
class GraphManager :
    public artis::pdevs::GraphManager<Time,
                                      QSSParameters<DerivativeParameters>,
                                      artis::common::NoParameters>
{
public:
  struct submodel
  {
    enum values
    {
      S_Derivative, S_Integrator, S_Quantifier
    };
  };

  struct input
  {
    enum values
    {
      RESET
    };
  };

  struct output
  {
    enum values
    {
      OUT
    };
  };

  GraphManager(common::Coordinator <Time> *coordinator,
               const QSSParameters<DerivativeParameters> &parameters,
               const artis::common::NoParameters &graph_parameters)
      :
      artis::pdevs::GraphManager<Time,
                                 QSSParameters<DerivativeParameters>,
                                 artis::common::NoParameters>(
          coordinator,
          parameters,
          graph_parameters
      ),
      _derivative("d", parameters.derivative),
      _integrator("i", parameters.integrator),
      _quantifier("q", parameters.quantifier)
  {
    this->add_child(submodel::S_Derivative, &_derivative);
    this->add_child(submodel::S_Integrator, &_integrator);
    this->add_child(submodel::S_Quantifier, &_quantifier);

    coordinator->input_port({input::RESET, "reset"});
    coordinator->output_port({output::OUT, "out"});

    this->in({coordinator, input::RESET})
        >> this->in({&_derivative, Derivative::input::RESET});
    this->in({coordinator, input::RESET})
        >> this->in({&_integrator, Integrator<Time>::input::RESET});
    this->in({coordinator, input::RESET})
        >> this->in({&_quantifier, Quantifier<Time>::input::RESET});

    this->out({&_derivative, Derivative::output::OUT})
        >> this->in({&_integrator, Integrator<Time>::input::X_DOT});
    this->out({&_integrator, Integrator<Time>::output::OUT})
        >> this->in({&_quantifier, Quantifier<Time>::input::IN});
    this->out({&_quantifier, Quantifier<Time>::output::OUT})
        >> this->in({&_integrator, Integrator<Time>::input::QUANTA});
    this->out({&_integrator, Integrator<Time>::output::OUT})
        >> this->out({coordinator, output::OUT});
    this->out({&_integrator, Integrator<Time>::output::OUT})
        >> this->in({&_derivative, Derivative::input::IN});
  }

  ~GraphManager() override = default;

  artis::pdevs::Simulator<Time, Derivative, DerivativeParameters> *
  derivative()
  { return &_derivative; }

private:
  artis::pdevs::Simulator<Time, Derivative, DerivativeParameters> _derivative;
  artis::pdevs::Simulator<Time, artis::qss::Integrator<Time>, artis::qss::IntegratorParameters>
      _integrator;
  artis::pdevs::Simulator<Time, artis::qss::Quantifier<Time>, artis::qss::QuantifierParameters>
      _quantifier;
};

}

#endif
